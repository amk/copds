require "compress/zip"
require "xml"
require "./book"

module OPDS::EPUB
  EPUB_NS = {"opf" => "http://www.idpf.org/2007/opf",
             "dc"  => "http://purl.org/dc/elements/1.1/",
             "rt"  => "urn:oasis:names:tc:opendocument:xmlns:container"}

  META_INF_CONTAINER = "META-INF/container.xml"

  class Error < Exception
  end

  class Epub < OPDS::AbstractBook
    @image_path : String?
    @path : Path
    getter title : String?
    getter author : String?

    def initialize(@path, @image_path, @title, @author)
    end

    def image_ext : String?
      image_path = @image_path
      if image_path
        File.extname image_path
      else
        nil
      end
    end

    def export_image(image_io : IO)
      image_path = @image_path
      unless image_path
        return
      end
      begin
        Compress::Zip::File.open(@path.to_s) do |file|
          cover = file[image_path]
          cover.open do |io|
            IO.copy io, image_io
          end
        end
      rescue ex : KeyError
        raise Error.new("Couldnt find key: #{ex}")
      end
    end

    def self.from_path(path : String)
      from_path(Path[path])
    end

    def self.from_path(path : Path)
      title = nil
      author = nil
      image_path = nil

      begin
        Compress::Zip::File.open(path.to_s) do |file|
          entry = file[META_INF_CONTAINER]
          content_obf_path = "content.obf"
          entry.open do |io|
            content_obf_path = XML.parse(io).xpath_string(
              "string(//rt:container/rt:rootfiles/rt:rootfile/@full-path)",
              namespaces: EPUB_NS)
          end
          obf_entry = file[content_obf_path]
          obf_entry.open do |io|
            document = XML.parse(io)
            title = document.xpath_string(
              "string(//opf:package/opf:metadata/dc:title)",
              EPUB_NS)
            author = document.xpath_string(
              "string(//opf:package/opf:metadata/dc:creator)",
              EPUB_NS)
            cover_id = document.xpath_string(
              "string(//opf:package/opf:metadata/opf:meta[@name=\"cover\"]/@content)",
              EPUB_NS)
            if cover_id == ""
              cover_id = "cover"
            end
            image_path = nil
            cover_node = document.xpath_node(
              "//opf:manifest/opf:item[@id=#{cover_id.dump}]",
              EPUB_NS)
            if cover_node
              parent = Path[content_obf_path].parent.to_s
              image_path = cover_node["href"]
              if !image_path.starts_with?("/") && parent != "."
                image_path = parent + "/" + cover_node["href"]
              end
            end
          end
        end
      rescue ex : XML::Error
        raise Error.new("Failed to parse epub: #{ex}")
      rescue ex : KeyError
        raise Error.new("Couldnt find key: #{ex}")
      end
      new(path, image_path, title, author)
    end
  end
end
