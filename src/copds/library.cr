require "xml"
require "uri"

require "./config"
require "./epub"

module OPDS
  VALID_TYPES = {".epub" => "application/epub+zip",
                 ".mobi" => "application/x-mobipocket-ebook",
                 ".pdf"  => "application/pdf",
                 ".png"  => "image/png",
                 ".jpg"  => "image/jpeg",
                 ".jpeg" => "image/jpeg"}

  IMAGE_TYPES = Set{".jpg", ".jpeg", ".png"}

  class Library
    property authors : Hash(String, Author)
    getter updated : Time
    @config : Config

    def initialize(@config)
      @authors = {} of String => Author
      @updated = Time.unix(0)
    end

    def self.index(config : Config)
      libr = Library.new(config)
      Dir.glob(config.library_path / "/**/*.*").each do |p|
        p_info = File.info p
        if (p_info && !p_info.file?) || !VALID_TYPES[File.extname p]?
          next
        end
        p = Path[p]
        file = p
        book = p.parent.basename
        author = p.parent.parent.basename
        libr.insert_author_and_publication(author, book, p)
      end
      libr
    end

    def to_opds
      to_opds(STDOUT)
    end

    def to_opds(io : IO)
      XML.build(io, indent: "   ") do |xml|
        xml.element("feed", {"xmlns"      => "http://www.w3.org/2005/Atom",
                             "xmlns:dc"   => "http://purl.org/dc/terms/",
                             "xmlns:opds" => "http://opds-spec.org/2010/catalog"}) do
          xml.element("link", {
            "rel"  => "self",
            "href" => "/opds.xml",
            "type" => "application/atom+xml;profile=opds-catalog;kind=acquisition",
          })
          xml.element("title") { xml.text "Latest" }
          xml.element("updated") { xml.text @updated.to_rfc3339 }
          xml.element("author") { xml.element("name") { xml.text "Generated Feed" } }
          to_newest_ordered_entries.each { |pub| pub.to_xml @config, xml }
        end
      end
    end

    protected def insert_author_and_publication(author : String, book : String, file : Path)
      info = File.info?(file)
      is_image = IMAGE_TYPES.includes?(File.extname file)
      if info.nil?
        return
      end
      unless @authors[author]?
        @authors[author] = Author.new(author)
      end
      author = @authors[author]
      unless author.publications[book]?
        author.publications[book] = Publication.new(author.name, book,
          info.modification_time)
      end
      book = author.publications[book]
      if !is_image && book.updated < info.modification_time
        book.updated = info.modification_time
      end
      if !is_image && info.modification_time > @updated
        @updated = info.modification_time
      end
      book.files << file
    end

    # add_book imports a file located at file_path, and copies it into
    # the library. book includes any meta information -- title, image etc.
    def add_book(file_path : Path, book : Book)
      final_dir = Path["#{@config.library_path}/#{book.author}/#{book.title}"]
      Dir.mkdir_p(final_dir)
      final_path = final_dir / Path["#{book.title} - #{book.author}#{File.extname file_path}"]
      if File.exists? final_path
        puts "File exists already, skipping"
        return
      end
      File.copy(file_path, final_path)
      # add the new book to the library tree
      insert_author_and_publication(book.author, book.title, final_path)
      # try to extract the cover from the book into the directory the
      # book was added to.
      extracted = extract_cover(final_dir, book)
      if extracted
        image_path = book.image_path
        if image_path
          insert_author_and_publication(book.author, book.title, image_path)
        end
      end
    end

    def extract_cover(publication_directory : Path, book : OPDS::Book) : Bool
      cover_ext = book.image_ext
      if cover_ext.nil? || !Dir.glob(publication_directory / "cover.*").empty?
        return false
      end
      cover_path = publication_directory / "cover#{cover_ext}"
      File.open(cover_path, mode: "w") do |io|
        book.export_image(io)
      end
      book.image_path = cover_path
      return true
    end

    def extract_covers
      inserted = [] of Book
      authors.values.each do |author|
        author.publications.values.each do |pub|
          pub.files.each do |pub_file|
            case File.extname pub_file
            when ".epub"
              begin
                book = Book.new(pub.title, pub.author, pub_file, EPUB::Epub.from_path(pub_file))
                new_image = extract_cover(Path[File.dirname pub_file], book)
                if new_image
                  inserted << book
                end
              rescue ex : EPUB::Error
                puts "Skipping #{pub_file}: #{ex}"
              end
            end
          end
        end
      end
      inserted.each { |insert|
        image_path = insert.image_path
        if image_path
          insert_author_and_publication(insert.author,
            insert.title,
            image_path)
        end
      }
    end

    def to_newest_ordered_entries : Array(Publication)
      authors.values.reduce([] of Publication) { |pubs, author|
        author.publications.values.each do |pub|
          pubs << pub
        end
        pubs
      }.sort_by { |pub| -pub.updated.to_unix }
    end
  end

  class Author
    property name : String
    property publications : Hash(String, Publication)

    def initialize(@name)
      @publications = {} of String => Publication
    end
  end

  class Publication
    property title : String
    property updated : Time
    property files : Array(Path)
    property author : String

    def to_xml(config, xml)
      xml.element("entry") do
        xml.element("author") { xml.element("name") { xml.text @author } }
        xml.element("title") { xml.text @title }
        xml.element("updated") { xml.text @updated.to_rfc3339 }
        @files.each do |file|
          ext = File.extname file
          href = URI.encode_path(
            Path["/"].join(file.relative_to(config.library_path)).to_s)
          rel = "http://opds-spec.org/acquisition"
          if File.basename(file).starts_with?("cover") && IMAGE_TYPES.includes?(ext)
            rel = "http://opds-spec.org/image"
          end
          xml.element("link", {
            "rel"  => rel,
            "href" => href,
            "type" => VALID_TYPES[ext],
          })
        end
      end
    end

    def initialize(@author, @title, @updated)
      @files = [] of Path
    end
  end
end
