require "ini"

module OPDS
  struct Config
    getter library_path : Path
    getter opds_path : Path

    def initialize(@library_path, @opds_path)
    end

    def self.from_ini(io : IO | String)
      config = INI.parse(io)
      Config.new(Path[config[""]["library_path"]],
        Path[config[""]["opds_path"]])
    end

    private def validate
      File.exists?(@library_path) || raise "Invalid library path"
    end
  end
end
