module OPDS
  abstract class AbstractBook
    abstract def title : String?
    abstract def author : String?
    abstract def image_ext : String?
    abstract def export_image(io : IO)
  end

  class Book
    getter title : String
    getter author : String
    # Full path to the ebook .epub/mobi etc
    getter path : Path
    # full path to the cover file
    property image_path : Path?
    @book : AbstractBook

    def initialize(@title, @author, @path, @book)
    end

    def image_ext : String?
      @book.image_ext
    end

    def export_image(io : IO)
      @book.export_image(io)
    end
  end
end
