require "xml"
require "uri"

require "option_parser"

require "./copds/library"
require "./copds/config"

module OPDS
  def self.regen_opds(cfg, library)
    File.open(cfg.opds_path, mode: "w") do |file|
      library.to_opds(file)
    end
  end

  def self.do_import(cfg, libr : OPDS::Library, file_import : Path)
    # TODO: make this more general and not epub only
    ready = false
    epub = EPUB::Epub.from_path(file_import.to_s)
    author = epub.author
    title = epub.title

    while !ready
      puts "
Author: \"#{author || ""}\"
Title: \"#{title || ""}\"

Modify (a)uthor, Modify (t)itle? <RETURN> to accept.
"

      print "> "

      input = gets
      exit if input.nil?
      case input
      when "a", "A"
        print "Author>"
        author = gets
      when "t", "T"
        print "Title>"
        title = gets
      when ""
        if author.nil? || author == ""
          puts "Invalid author"
          next
        end
        if title.nil? || title == ""
          puts "Invalid title"
          next
        end
        book = Book.new(title, author, file_import, epub)
        libr.add_book(file_import, book)
        return
      else
        puts "Invalid input \"#{input}\""
      end
    end
  end

  def self.main
    config_path = "/etc/copds.ini"
    extract = false
    file_import = ""

    OptionParser.parse do |parser|
      parser.banner = "Usage: copds [arguments]"
      parser.on("-c PATH", "--config=PATH",
        "Specify path to config file") { |path| config_path = path }
      parser.on("-e", "--extract-covers",
        "Extract covers from epubs during generation") { extract = true }
      parser.on("-i FILE", "--import-file=FILE",
        "Import a specific epub") { |path| file_import = path }
      parser.on("-h", "--help", "Show this help") do
        puts parser
        exit
      end
      parser.invalid_option do |flag|
        STDERR.puts "ERROR: #{flag} is not a valid option."
        STDERR.puts parser
        exit(1)
      end
    end

    config = nil
    File.open(config_path) do |cfg_file|
      config = Config.from_ini(cfg_file)
    end
    config = config || raise "Failed to parse config file"
    libr = Library.index(config)

    case
    when extract
      puts "Extracting covers from existing books."
      libr.extract_covers
    when file_import != ""
      do_import(config, libr, Path[file_import])
    end
    regen_opds(config, libr)
    puts "Wrote refreshed #{config.opds_path}"
  end
end

OPDS.main
