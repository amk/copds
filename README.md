# cOPDS

## Building 

Requires crystal and libxml2-dev, currently tested with 1.2.2

```
shards build
```


## Usage 

A very simple OPDS feed generator.

Simply converts a directory tree into an opds feed ordered by last modified.

When importing a file it will attempt to follow the AUTHOR/TITLE/FILE
directory layout.


```
Usage: copds [arguments]
    -c PATH, --config=PATH           Specify path to config file
    -e, --extract-covers             Extract covers from epubs during generation
    -i FILE, --import-file=FILE      Import a specific epub
    -h, --help                       Show this help	
```

This requires that your library is layed out like {Author}/{Book Title}/{Files} --

```
Library/
├── Andrzej Sapkowski
│   └── The Last Wish
│       └── The Last Wish - Andrzej Sapkowski.epub
├── Craig A. Finseth
│   └── The Craft of Text Editing
│       └── The Craft of Text Editing - Craig A. Finseth.epub
├── David Graeber
│   └── Bullshit Jobs
│       └── Bullshit Jobs - David Graeber.epub
├── Frank Herbert
│   └── Dune
│       └── Dune - Frank Herbert.epub
├── Shamus Young
│   └── Free Radical
│       ├── Free Radical - Shamus Young.epub
│       └── Free Radical - Shamus Young.mobi
└── H.P. Lovecraft
    └── Necronomicon
        └── Necronomicon - H.P. Lovecraft.epub
```

The copds.ini will default to /etc/copds.ini and has the following configuration options:

```ini
library_path=<path to library>
opds_path=<path to place opds.xml>
```
